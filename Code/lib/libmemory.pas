unit libMemory;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil;

type
  
  { transport_array }

  transport_array = class
    content : array of string;
    constructor create(x : integer);
  end;

function read_charackter(path : string): transport_array;
function read_map(path : string): transport_array;
function read_units(path : string):transport_array;

implementation

function read_charackter(path: string): transport_array;
var
  data : text;
  i : integer;
  reader : string;
begin
  result := transport_array.create(8);
  Assign(data, 'stats/'+path+'.char');
  Reset(data);
  i := 1;
  repeat
    Readln(data, reader);
    result.content[i] := reader;
    i += 1;
  until i = 8;
  Close(data);
end;

function read_map(path: string): transport_array;
var
  data : text;
  i : integer;
  reader : string;
begin
  result := transport_array.create(21);
  Assign(data, 'env/'+path+'.map');
  Reset(data);
  i := 1;
  repeat
    Readln(data, reader);
    result.content[i] := reader;
    i += 1;
  until i >= 21;
  Close(data);
end;

function read_units(path: string): transport_array;
var
  data : text;
  i : integer;
  reader : string;
begin
  result := transport_array.create(300);
  Assign(data, 'env/'+path+'.units');
  Reset(data);
  i := 1;
  repeat
    Readln(data, reader);
    result.content[i] := reader;
    i += 1;
  until reader = 'EOF';
  Close(data);
end;

{ transport_array }

constructor transport_array.create(x: integer);
begin
  setLength(self.content, x);
end;

end.

