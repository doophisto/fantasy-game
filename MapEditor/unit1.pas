unit Unit1;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls, unit2;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    ListBox1: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseEnter(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    drawing : boolean;
    field : array [1..20,1..15] of TImage;
    maper : array [1..20,1..15] of string;
    const pix : integer = 50;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var x, y: integer;
begin
  for x := 1 to 20 do
    for y := 1 to 15 do
      begin
      Field[X,Y]:= TImage.create(Form1);
      Field[X,Y].top := (y-1)* self.pix;
      Field[X,Y].left := (x-1)* self.pix;
      Field[X,Y].Show;
      Field[X,Y].Parent:=Form1;
      Field[X,Y].Height:=self.pix;
      Field[X,Y].Width:=self.pix;
      self.field[x,y].Canvas.Clear;
      self.field[x,y].OnMouseDown:=@Form1.FormMouseDown;
      self.field[x,y].OnMouseEnter:=@Form1.FormMouseEnter;
      self.field[x,y].OnMouseUp:=@Form1.FormMouseUp;
      self.maper[x,y] := '11';
      end;
  self.ListBox1.Items.Append('00');
  self.ListBox1.Items.Append('01');
  self.ListBox1.Items.Append('02');
  self.ListBox1.Items.Append('03');
  self.ListBox1.Items.Append('04');
  self.ListBox1.Items.Append('05');
  self.ListBox1.Items.Append('06');
  self.ListBox1.Items.Append('07');
  self.ListBox1.Items.Append('08');
  self.ListBox1.Items.Append('09');
  self.ListBox1.Items.Append('0A');
  self.ListBox1.Items.Append('0B');
  self.ListBox1.Items.Append('0C');
  self.ListBox1.Items.Append('0D');
  self.drawing := false;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if self.drawing then
    self.drawing:=false
  else
    self.drawing:=true;
end;

procedure TForm1.FormMouseEnter(Sender: TObject);
var img : TImage;
  mapx,mapy : integer;
begin
  if self.drawing then begin
    mapX := (mouse.CursorPos.x - self.left) DIV self.pix +1;
    mapY := (mouse.CursorPos.y - self.top ) DIV self.pix +1;
    img := self.field[mapx,mapy];
    img.Picture.LoadFromFile('img/'+self.ListBox1.GetSelectedText+'.png');
    self.maper[mapx,mapy] := self.ListBox1.GetSelectedText;
  end;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TForm1.Button1Click(Sender: TObject);
var x,y : integer;
  res : string;
begin
  for x:=1 to 20 do begin
    res := '';
    for y := 1 to 15 do
      res += self.maper[x,y];
    Form2.Memo1.Append(res);
  end;
  Form2.Show;
end;

end.

