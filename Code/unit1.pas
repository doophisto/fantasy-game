unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  libMap, libCharackters, LCLType, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm) //20 x 15 Feld + Buttons
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ChangePlayer;
    procedure FormCreate();
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { private declarations }
  public
    Field: Array [1..20, 1..15] of TImage;
    map : c_map;
    Activeplayer: integer;  //
    selected_pic : TPicture;
    unit_move : boolean; // ist true wenn der spieler eine Einheit bewegt
    selX,selY : integer;
    uX, uY : integer;
    att, def : CCharackters;
    moving_unit : CCharackters;
    cover : integer;
    attacking : boolean;
    procedure gen;
    procedure refresh;
    procedure Click(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ShowFightScreen(at, de : CCharackters; cove : integer);
    { public declarations }
  end;

var
  Form1: TForm1;  //1000 x 800 pixel
const pix : integer = 50;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate();
begin
  self.selected_pic := TPicture.create;
  self.selected_pic.LoadFromFile('img/select.png');
  self.unit_move:=false;
  self.GroupBox1.Hide;
  self.Activeplayer:=1;
  self.attacking:=false;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  damage : integer;
begin
  damage := self.att.attack * fightc(self.att.clas, self.def.clas) - self.def.defence;
  if damage <= 0 then
    damage := 0;
  self.def.live-= damage;
  self.unit_move:=false;
  if not(self.attacking) then begin
    self.map.field[self.selx, self.sely].Unit_on_it := self.moving_unit;
    self.map.field[self.uX, self.uY].Unit_on_it := nil;
  end else begin
    self.map.field[self.uX,self.uY].Unit_on_it := self.moving_unit;
  end;
  self.moving_unit := nil;
  self.refresh;
  self.GroupBox1.Hide;
end;

procedure TForm1.ChangePlayer;
var
  x,y : integer;
begin
  if self.Activeplayer = 2 then
    self.Activeplayer:=1
  else
    self.Activeplayer:=2;
  for x := 1 to 20 do
    for y := 1 to 15 do
      if self.map.field[x,y].Unit_on_it <> nil then
        self.map.field[x,y].Unit_on_it.moved:=false;
  self.refresh;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  self.GroupBox1.Hide;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
  );
begin
  if key = VK_TAB then
    self.ChangePlayer;
  if self.unit_move then begin
    if key = VK_F then
      if (self.moving_unit.name = 'Artillerie') or (self.moving_unit.clas = 'Flugzeug') then
        self.attacking:=true;
    if key = VK_ESCAPE then begin
      self.unit_move := false;
      self.moving_unit.moved:=false;
      self.moving_unit := nil;
      self.refresh;
    end;
    if key = VK_RETURN then begin   // stopt das bewegen der einheit
      if self.unit_move then begin
        self.unit_move:=false;
        self.map.field[self.selx, self.sely].Unit_on_it := self.moving_unit;
        self.map.field[self.uX, self.uY].Unit_on_it := nil;
        self.moving_unit := nil;
        self.refresh;
      end;
    end
    else if key = VK_UP then begin
      if ((self.moving_unit.speed - self.map.field[self.selx,self.sely-1].get_speed(self.moving_unit.clas)) >= 0) and self.map.field[self.selx,self.sely-1].Acceseable then begin
        if (self.map.field[self.selx,self.sely-1].Unit_on_it <> nil) and (self.map.field[self.selX,self.sely-1].Unit_on_it.side <> self.moving_unit.side) then
          ShowFightScreen(self.moving_unit, self.map.field[self.selx, self.sely-1].Unit_on_it, self.map.field[self.selx, self.sely-1].cover)
        else
          begin
            self.sely -=1;
            if self.map.field[self.selx, self.sely].selected then begin
              self.map.field[self.selx, self.sely].selected := false;
              self.Field[self.selx, self.sely+1].Canvas.Clear;
              self.moving_unit.speed += self.map.field[self.selx, self.sely+1].get_speed(self.moving_unit.clas);
              self.Field[self.selx, self.sely+1].Canvas.Draw(0,0,self.map.field[self.selx,self.sely+1].Background.Graphic);
            end
            else begin
              self.Field[self.selX,self.sely].Canvas.Draw(0,0,self.selected_pic.Graphic);
              self.map.field[self.selx,self.sely].selected:= true;
              self.moving_unit.speed -= self.map.field[self.selx,self.sely].get_speed(self.moving_unit.clas);
            end;
          end;
      end;
    end
    else if key = VK_DOWN then begin
      if ((self.moving_unit.speed - self.map.field[self.selx,self.sely+1].get_speed(self.moving_unit.clas)) >= 0) and self.map.field[self.selx,self.sely+1].Acceseable then begin
        if (self.map.field[self.selx,self.sely+1].Unit_on_it <> nil) and (self.map.field[self.selX,self.sely+1].Unit_on_it.side <> self.moving_unit.side) then
          ShowFightScreen(self.moving_unit, self.map.field[self.selx, self.sely+1].Unit_on_it, self.map.field[self.selx, self.sely+1].cover)
        else
          begin
            self.sely +=1;
            if self.map.field[self.selx, self.sely].selected then begin
              self.map.field[self.selx, self.sely].selected := false;
              self.moving_unit.speed += self.map.field[self.selx, self.sely-1].get_speed(self.moving_unit.clas);
              self.Field[self.selx, self.sely-1].Canvas.Clear;
              self.Field[self.selx, self.sely-1].Canvas.Draw(0,0,self.map.field[self.selx,self.sely-1].Background.Graphic);
            end
            else begin
              self.Field[self.selX,self.sely].Canvas.Draw(0,0,self.selected_pic.Graphic);
              self.map.field[self.selx,self.sely].selected:= true;
              self.moving_unit.speed -= self.map.field[self.selx,self.sely].get_speed(self.moving_unit.clas);
            end;
          end;
      end;
    end
    else if key = VK_LEFT then begin
      if ((self.moving_unit.speed - self.map.field[self.selx-1,self.sely].get_speed(self.moving_unit.clas)) >= 0) and self.map.field[self.selx-1,self.sely].Acceseable then begin
        if (self.map.field[self.selx-1,self.sely].Unit_on_it <> nil) and (self.map.field[self.selX-1,self.sely].Unit_on_it.side <> self.moving_unit.side) then
          ShowFightScreen(self.moving_unit, self.map.field[self.selx-1, self.sely].Unit_on_it, self.map.field[self.selx-1, self.sely].cover)
        else
          begin
            self.selx -=1;
            if self.map.field[self.selx, self.sely].selected then begin
              self.map.field[self.selx, self.sely].selected := false;
              self.Field[self.selx+1, self.sely].Canvas.Clear;
              self.moving_unit.speed += self.map.field[self.selx+1, self.sely].get_speed(self.moving_unit.clas);
              self.Field[self.selx+1, self.sely].Canvas.Draw(0,0,self.map.field[self.selx+1,self.sely].Background.Graphic);
            end
            else begin
              self.Field[self.selX,self.sely].Canvas.Draw(0,0,self.selected_pic.Graphic);
              self.map.field[self.selx,self.sely].selected:= true;
              self.moving_unit.speed -= self.map.field[self.selx,self.sely].get_speed(self.moving_unit.clas);
            end;
          end;
      end;
    end
    else if key = VK_RIGHT then begin
      if ((self.moving_unit.speed - self.map.field[self.selx+1,self.sely].get_speed(self.moving_unit.clas)) >= 0) and self.map.field[self.selx+1,self.sely].Acceseable then begin
        if (self.map.field[self.selx+1,self.sely].Unit_on_it <> nil) and (self.map.field[self.selX+1,self.sely].Unit_on_it.side <> self.moving_unit.side) then
          ShowFightScreen(self.moving_unit, self.map.field[self.selx+1, self.sely].Unit_on_it, self.map.field[self.selx+1, self.sely].cover)
        else
          begin
            self.selX +=1;
            if self.map.field[self.selx, self.sely].selected then begin
              self.map.field[self.selx, self.sely].selected := false;
              self.Field[self.selx-1, self.sely].Canvas.Clear;
              self.moving_unit.speed += self.map.field[self.selx-1, self.sely].get_speed(self.moving_unit.clas);
              self.Field[self.selx-1, self.sely].Canvas.Draw(0,0,self.map.field[self.selx-1,self.sely].Background.Graphic);
            end
            else begin
              self.Field[self.selX,self.sely].Canvas.Draw(0,0,self.selected_pic.Graphic);
              self.map.field[self.selx,self.sely].selected:= true;
              self.moving_unit.speed -= self.map.field[self.selx,self.sely].get_speed(self.moving_unit.clas);
            end;
          end;
      end;
    end;
  end;
end;

procedure TForm1.gen;
var x, y: integer;
begin
  for x := 1 to 20 do
    for y := 1 to 15 do
      begin
      Field[X,Y]:= TImage.create(Form1);
      Field[X,Y].top := (y-1)* pix;
      Field[X,Y].left := (x-1)* pix;
      Field[X,Y].Show;
      Field[X,Y].Parent:=Form1;
      Field[X,Y].Height:=pix;
      Field[X,Y].Width:=pix;
      self.Field[x,y].OnMouseDown:=@Form1.Click;
      end;
  self.refresh;
  end;

procedure TForm1.refresh;
var x,y : integer;
begin
  self.map.refresh;
  for x := 1 to 20 do
    for y := 1 to 15 do
      begin
      self.map.field[x,y].selected:=false;
      self.Field[x, y].canvas.Draw(0,0,self.map.field[x,y].Background.Graphic);
      if self.map.field[x,y].Unit_on_it <> nil then
        self.Field[x,y].canvas.draw(0,0,self.map.field[x,y].Unit_on_it.img.Graphic);
      end;
end;

procedure TForm1.Click(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var
  mapX,mapY : integer;
  entfernung : real;
begin
  mapX := (mouse.CursorPos.x - self.left) DIV pix +1;
  mapY := (mouse.CursorPos.y - self.top ) DIV pix +1;
  if not(self.unit_move) and not(self.attacking) then begin
    if self.map.field[mapx, mapy].Unit_on_it <> nil then
      if not(self.map.field[mapx, mapy].Unit_on_it.moved) and (self.map.field[mapx, mapy].Unit_on_it.side = self.Activeplayer) then begin
        self.map.field[mapx, mapy].Unit_on_it.moved:=true;
        self.unit_move:=true;
        self.selx := mapx;
        self.sely := mapy;
        self.ux := self.selx;
        self.uy := self.sely;
        self.moving_unit := self.map.field[mapx,mapy].Unit_on_it;
        self.Field[mapx,mapy].canvas.draw(0,0,self.selected_pic.Graphic);
        self.Label6.Caption:=self.map.field[mapx,mapy].Unit_on_it.name;
      end
      else
        showmessage ('Die Einheit wurde bereits bewegt, oder gehört einenm anderen Spieler')
    else
      showmessage ('Hier ist keine Einheit.');
  end else if self.attacking then begin
    entfernung := sqrt(abs(self.selX - mapx)*abs(self.selX - mapx)+abs(self.selY - mapy)*abs(self.selY - mapy));
    if entfernung <= 5 then
      ShowFightScreen(self.moving_unit, self.map.field[mapx, mapy].Unit_on_it, self.map.field[mapx,mapy].cover);
  end;
end;

procedure TForm1.ShowFightScreen(at, de : CCharackters; cove: integer);
begin
  self.att := at; self.def := de; self.cover:=cove;
  self.Image1.Picture:=att.img;
  self.Label2.Caption:='Angreifer: '+IntToStr(self.att.attack);
  //Form3.att:=att;
  self.Image2.Picture:=def.img;
  self.Label3.Caption:='Verteidiger: '+IntToStr(self.def.defence);
  //Form3.deff:=def;
  self.Label4.Caption:='Deckung: '+IntToStr(self.cover);
  self.Label5.Caption:='Leben: '+IntToStr(self.def.live);
  self.GroupBox1.Show;
end;

end.

