Datenbank aussehen
==================

Standart DB
===========

Tabelle Units:
--------------
Name, Klasse, Rasse, IMG, {alles hiernach wird ignoriert wenn nicht gesetzt}, Lp, Angriff, PVerteidigung, MVerteidigung, Tempo, Reichweite, Schwächen, unpack_needed, fight_after_move 

Tabelle Klassen:
----------------
KlasseName, Leben, Angrff, PVerteidugung, MVerteidigung, Tempo, Reichweite, Schwächen, FallbackIMG, unpack_needed, fight_after_move
unpack_needed : default false; fight_after_move : default true;

Tabelle Rasse:
--------------
RasseName, Leben, Angriff, PVerteidigung, MVerteidigung, Tempo, Schwächen

Tabelle Gebäude:
----------------
name, defense, movabel, special_ability, special_ablity_range, IMG

Tabelle Untergrund:
-------------------
name, speed_points, movabel, strasse

Save DB
=======

Tabelle Units:
--------------
Name, Rasse, lp, packed, x, y, owner

Tabelle Gebäude:
----------------
Name, destroyed, owner
