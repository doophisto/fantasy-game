unit libMap;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, libMemory, libCharackters;

type

  { C_Sektor }

  C_Sektor = class
    Background : TPicture;
    Unit_on_it : CCharackters;
    cover : integer;
    Acceseable : boolean;
    selected : boolean;
    typ : string;
    constructor create(typp : string);
    function get_speed(klass : string):integer;
  end;

  { C_Map }

  C_Map = class
    private
      //
    public
      preloaded_units : array of CCharackters;
      field : array [1..20, 1..15] of C_Sektor;
      constructor create (inPath : string);
      function move_charackter(inX, inY, inX2, inY2 : integer):boolean;
      function fight(posAI, posAII, posBI, posBII : integer):integer;
      procedure refresh;
      procedure create_unit(posI, posII, unit_number, side: integer);
  end;

implementation

{ C_Sektor }

constructor C_Sektor.create(typp: string);
begin
  self.typ := typp;
  self.Background := TPicture.Create;
  self.background.LoadFromFile('img/'+typp+'.png');
  self.Acceseable:=true;
  self.selected:=false;
  if (typp = '02') or (typp = '04') or (typp = '09') then
    self.Acceseable:=false;
end;

function C_Sektor.get_speed(klass: string): integer;
begin
  result := 1; //default
  if typ = '03' then
    if klass = 'Fahrzeug' then
      result := 2;
end;

{ C_Map }

constructor C_Map.create(inPath: string);
var
  x, y, i : integer;
  r_map : transport_array;
  people : transport_array;
  ac,dc : string;
begin
  r_map := read_map(inPath);
  for x := 1 to 20 do
    for y := 1 to 15 do begin
      self.field[x, y] := C_sektor.create(r_map.content[x][y*2-1]+r_map.content[x][y*2]);
    end;
  people := read_units(inPath);
  for i := 1 to 150 do begin
    ac := people.content[i*2-1];
    if ac = 'EOF' then
      break;
    dc := people.content[i*2];
    self.field[StrToint(ac[1]+ac[2]),StrToint(ac[3]+ac[4])].Unit_on_it:=CCharackters.create(dc, StrToInt(ac[5]));
  end;
end;

function C_Map.move_charackter(inX, inY, inX2, inY2 : integer): boolean;
var
  fieldI, fieldII : C_Sektor;
begin
  fieldI := self.field[inX, inY];
  fieldII := self.field[inX2, inY2];
  if fieldII.Unit_on_it = nil then begin
    fieldII.Unit_on_it := fieldI.Unit_on_it;
    fieldI.Unit_on_it := nil;
  end else
    result := false;
end;

function C_Map.fight(posAI, posAII, posBI, posBII: integer): integer;
var
  posA, posB : C_Sektor;
  damage : real;
begin
  posA := self.field[posAI, posAII];  // Angreifer
  posB := self.field[posBI, posBII];  // Verteidiger
  damage := posA.Unit_on_it.attack * fightC(posA.Unit_on_it.clas, posB.Unit_on_it.clas) - posB.Unit_on_it.defence;
  posA.Unit_on_it.live -= round(damage);
  result := round(damage);
end;

procedure C_Map.refresh;
var
  i, j : integer;
begin
  for i := 1 to 20 do
    for j := 1 to 15 do begin
      if self.field[i,j].Unit_on_it <> nil then
        if not (self.field[i, j].Unit_on_it.refresh) then
          self.field[i,j].Unit_on_it:= nil;
    end;
end;

procedure C_Map.create_unit(posI, posII, unit_number, side: integer);
begin
  if self.field[posI, posII].Unit_on_it = nil then
    begin
      self.field[posI, posII].Unit_on_it.clone(self.preloaded_units[unit_number]);
      self.field[posI, posII].Unit_on_it.side := side;
    end;
end;

end.
