unit libCharackters;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, libmemory;

type
  
  { CCharackters }

  CCharackters = class
    public
      attack, defence, speed, side, max_speed: integer;
      name, clas : string;
      img : TPicture;
      moved : boolean;
    //public
      live : integer;
      constructor create (c_file :string; inside : integer);
      constructor clone(inp : CCharackters);
      destructor destroy;
      function refresh:boolean;
  end;

function fightC(att, def : string):integer;

implementation

function fightC(att, def : string):integer;
begin
  result := 1; // Wenn nichts gillt, dann das
  // gibt den Multiplikator für den Schaden aus
  if (att = 'FALK') and (def = 'Flugzeug') then
    result := 3;
end;

{ CCharackters }

constructor CCharackters.create(c_file : string; inside : integer);
var
  inf : transport_array;
begin
  inf := read_charackter(c_file);
  self.attack          := strtoint(inf.content[1]);
  self.defence         := strtoint(inf.content[2]);
  self.speed           := strtoint(inf.content[3]);
  self.name            := inf.content[4];
  self.clas            := inf.content[5];
  self.live            := strtoint(inf.content[6]);
  self.img             := TPicture.create;
  self.img.LoadFromFile  ('img/'+inf.content[7]);
  self.side            := inside;
  self.moved           := false;
  self.max_speed:=self.speed;
end;

constructor CCharackters.clone(inp: CCharackters);
begin
  self.attack := inp.attack;
  self.img:=inp.img;
  self.live:=inp.live;
  self.defence:=inp.defence;
  self.speed:=inp.speed;
  self.name := inp.name;
  self.clas := inp.clas;
  self.moved           := false;
  self.max_speed:=self.speed;
end;

destructor CCharackters.destroy;
begin
end;

function CCharackters.refresh: boolean;
begin
  if self.live <= 0 then begin
    result := false;
    self.destroy;
  end
  else
    result := true;
  self.speed:=self.max_speed;
end;

end.

