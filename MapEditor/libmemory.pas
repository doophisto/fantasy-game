unit libmemory;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil;

type
  
  { FileHandler }

  FileHandler = class
    public
      //data : text;
      constructor create(path,  opt : string);
      procedure write(content:string);
      function read():string;
      destructor destroy();
    private
      opt : string;

  end;

implementation

{ FileHandler }

constructor FileHandler.create(path, opt: string);
var data : text;
begin
  Assign(data, 'fle1');
  //Reset(data);
  {writeln('---');
  if opt = 'r' then
    reset(self.data)
  else
    ReWrite(self.data);
  writeln('---'); }
  close(data);
end;

procedure FileHandler.write(content: string);
begin
  {if opt = 'w' then
    writeln(self.data, content);  }
end;

function FileHandler.read(): string;
begin
  {if opt = 'r' then
    readln(self.data, result);   }
end;

destructor FileHandler.destroy();
begin
  {close(self.data);}
end;

end.

