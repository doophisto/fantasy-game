unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Unit1, libMap;

type

  { TForm2 }

  TForm2 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    ListBox1: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

{ TForm2 }

procedure TForm2.Button1Click(Sender: TObject);
begin
  Form1.map := C_Map.create(ListBox1.GetSelectedText);
  Form1.gen;
  Form1.show;
  Form2.hide;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  ListBox1.Items.Append('debug');
  ListBox1.Items.Append('map1');
end;

end.

